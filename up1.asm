	.ORIG	x3000

	;Initialization			
	AND	R0, R0, #0
	AND	R1, R1, #0
	AND	R2, R2, #0
	AND	R3, R3, #0
	AND	R4, R4, #0
	AND	R5, R5, #0
	AND	R6, R6, #0
	AND	R7, R7, #0
	
	LD	R1, TRAP26	;Store x3300 to R1
	STI	R1, TRAPVEC	;Store Trap26 routine location x3300 to x0026 
	LD	R1, ISR		;Store x3500 to R1
	STI	R1, ISRVEC	;Store ISR location x3500 to x0180
	LD	R6, TOP		;Start top of the stack R6 at x3000
	
	;Program starts		
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen
	TRAP	x26  ; get char
	TRAP	x21  ; print char to screen

	HALT

TOP	.FILL	x3000
TRAPVEC	.FILL	x0026
TRAP26	.FILL	x3300
ISRVEC	.FILL	x0180
ISR	.FILL	x3500

	.END
