	.ORIG	x3300
	
	STI	R7, UP1SAVE	;Store R7, the PC to x32FF
	LD	R1, IEBIT	;Save interrupt enable bit into R1
	LD	R2, UP2LOC	;Save location of UP2 into R2
	STI 	R1, KBSRLOC	;Set KBSR interruptable bit (14) to 1
	JMP	R2		;Jump to UP2 	

UP2LOC	.FILL	x3400
IEBIT	.FILL	x4000
R1SAVE 	.FILL	#0
R2SAVE	.FILL	#0
UP1SAVE	.FILL	x32FF
KBSRLOC	.FILL	xFE00

	.END