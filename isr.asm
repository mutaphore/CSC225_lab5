	.ORIG	x3500
	
	LDI	R0, KBDRLOC	;Load R0 with data in KBDR
	AND	R5, R5, #0	;Clear R5
	STI	R5, KBSRLOC	;Clear KBSR
 	LDI	R5, UP1SAVE	;Load UP1 PC into R5
	STR	R5, R6, #0	;Load the top stack pointer R6 with PC at UP1	
	RTI			;Return to UP1
	
KBSRLOC .FILL	xFE00
KBDRLOC	.FILL	xFE02
UP1SAVE	.FILL	x32FF

	.END
